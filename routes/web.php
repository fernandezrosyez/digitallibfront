<?php

use Illuminate\Support\Facades\Route;
use Phinx\Db\Table\Index;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


//Route::get('/{id}/{chapters_id}/{topic_id}', 'FrontendController@chapterlist')->name('Chapter');

Auth::routes();

Route::prefix('admin')->group(function () {
    Route::get('/dashboard', 'DashboardsController@index');
    Route::get('/login', 'UsersController@login');

    //////////////////// Subject Management ////////////////////
    Route::get('/add-subject', ['uses'=> 'SubjectsController@add', 'as' => 'add-subject']);
    Route::post('/create-subject', ['uses'=> 'SubjectsController@createSubject', 'as' => 'createSubject']);
    Route::get('/edit-subject/{id}', ['uses'=> 'SubjectsController@edit', 'as' => 'edit-subject']);
    Route::post('/update-subject/{id}', ['uses'=> 'SubjectsController@update', 'as' => 'update-subject']);
    Route::get('/list-subject', ['uses'=> 'SubjectsController@index', 'as' => 'list-subject']);
    Route::get('/status-subject/{id}/{status}', ['uses'=> 'SubjectsController@statusChange', 'as' => 'status-subject']);
    Route::get('/delete-subject/{id}', ['uses'=> 'SubjectsController@delete', 'as' => 'delete-subject']);

    //////////////////// Chapter Management ////////////////////
    Route::get('/add-chapter', ['uses'=> 'ChaptersController@add', 'as' => 'add-chapter']);
    Route::post('/create-chapter', ['uses'=> 'ChaptersController@createChapter', 'as' => 'createchapter']);
    Route::get('/edit-chapter/{id}', ['uses'=> 'ChaptersController@edit', 'as' => 'edit-chapter']);
    Route::post('/update-chapter/{id}', ['uses'=> 'ChaptersController@update', 'as' => 'update-chapter']);
    Route::get('/list-chapter', 'ChaptersController@index')->name('list-chapter');
    Route::get('/status-chapter/{id}/{status}', ['uses'=> 'ChaptersController@statusChange', 'as' => 'status-chapter']);
    Route::get('/delete-chapter/{id}', ['uses'=> 'ChaptersController@delete', 'as' => 'delete-chapter']);

    //////////////////// Topic Management ////////////////////
    Route::get('/add-topic', ['uses'=> 'TopicsController@add', 'as' => 'add-topic']);
    Route::post('/create-topic', ['uses'=> 'TopicsController@createTopic', 'as' => 'createtopic']);
    Route::get('/get-chapters', ['uses'=> 'TopicsController@getChapters', 'as' => 'get-chapters']);
    Route::get('/list-topic', 'TopicsController@index')->name('list-topic');
    Route::get('/edit-topic/{id}', ['uses'=> 'TopicsController@edit', 'as' => 'edit-topic']);
    Route::post('/update-topic/{id}', ['uses'=> 'TopicsController@update', 'as' => 'update-topic']);
    Route::get('/status-topic/{id}/{status}', ['uses'=> 'TopicsController@statusChange', 'as' => 'status-topic']);
    Route::get('/delete-topic/{id}', ['uses'=> 'TopicsController@delete', 'as' => 'delete-topic']);

    //////////////////// Example Management ////////////////////
    Route::get('/add-example', ['uses'=> 'ExampleController@add', 'as' => 'add-example']);
    Route::post('/create-example', ['uses'=> 'ExampleController@createExample', 'as' => 'createExample']);
    //Route::get('/get-chapters', ['uses'=> 'ExampleController@getAllChapters', 'as' => 'get-all-chapters']);
    Route::post('/get-all-chapter',['as'=>'get-all-chapters', 'uses'=>'ExampleController@getAllChapters']);
    Route::post('/get-all-topic',['as'=>'get-all-topics', 'uses'=>'ExampleController@getAllTopics']);
    Route::get('/edit-example/{id}', ['uses'=> 'ExampleController@edit', 'as' => 'edit-example']);
    Route::post('/update-example/{id}', ['uses'=> 'ExampleController@update', 'as' => 'update-example']);
    Route::get('/list-example', ['uses'=> 'ExampleController@index', 'as' => 'list-example']);
    Route::get('/status-example/{id}/{status}', ['uses'=> 'ExampleController@statusChange', 'as' => 'status-example']);
    Route::get('/delete-example/{id}', ['uses'=> 'ExampleController@delete', 'as' => 'delete-example']);

    Route::get('/add-faq', ['uses'=> 'FaqController@add', 'as' => 'add-faq']);
    Route::post('/create-faq', ['uses'=> 'FaqController@createFaq', 'as' => 'create-faq']);

    Route::get('/edit-faq/{id}', ['uses'=> 'FaqController@edit', 'as' => 'edit-faq']);
    Route::post('/update-faq/{id}', ['uses'=> 'FaqController@update', 'as' => 'update-faq']);
    Route::get('/list-faq', ['uses'=> 'FaqController@index', 'as' => 'list-faq']);
    Route::get('/status-faq/{id}/{status}', ['uses'=> 'FaqController@statusChange', 'as' => 'status-faq']);
    Route::get('/delete-faq/{id}', ['uses'=> 'FaqController@delete', 'as' => 'delete-faq']);



});

Route::get('logout', 'UsersController@login@logout');
////////// End Of Admin Routing //////////


////////// Home Page Routing Starts Here //////////
Route::get('/', ['as' => 'index', 'uses' => 'FrontendController@index']);
Route::get('/example', ['as' => 'example', 'uses' => 'FrontendController@example']);
Route::get('/example-details/{slug}', 'FrontendController@exampleDetails')->name('example-details');
Route::get('/faq', 'FrontendController@faq')->name('faq');
Route::get('/faq-details/{slug}', 'FrontendController@faqDetails')->name('faq-details');

//Route::get('/{subject_id}', 'FrontendController@index')->name('Home');
Route::get('/{slug}', 'FrontendController@Subjectindex')->name('Subject');
Route::get('/{id}/{chapters_id}', 'FrontendController@chapterlist')->name('Chapter');

Route::get('/{slug}/{chapSlug}/{topicSlug}', 'FrontendController@chapterlist')->name('Chapter');
////////// End Of Home Page Routing //////////


<?php

namespace App\Http\Controllers;

use App\FlatDetailsModel;
use Illuminate\Http\Request;
use App\Topic;
use Cviebrock\EloquentSluggable\Services\SlugService;
use DB;

class TopicsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $topics = Topic::select('id', 'subject_id', 'chapter_id', 'title', 'isActive', 'created_at')
            ->orderBy('created_at', 'desc')
            ->with(['subject', 'chapter'])->get();
            //dd($topics);

        return view('topics/index', ['topics' => $topics]);
    }

    public function add()
    {
        $subjects = DB::table('subjects')
                        ->select('id', 'title','featured')
                        ->where('isActive', '1')
                        ->orderBy('title')
                        ->get();
        return view('topics/add', ['subjects'=>$subjects]);
    }

    public function getChapters(Request $request)
    {
        $subject_id = $request->sub_id;
        //dd($subject_id);
        $chapters = DB::table('chapters')
                        ->select('id', 'title')
                        ->where('isActive', '1')
                        ->where('subject_id', $subject_id)
                        ->orderBy('title')
                        ->get();
        //dd($chapters);
        return response()->json($chapters);
    }

    public function createtopic(Request $request)
    {
        $slug = SlugService::createSlug(Topic::class, 'slug', $request->title);
        //dd($request->content);
        $chapter = Topic::create([
            'subject_id' => $request->subject_id,
            'chapter_id' => $request->chapter_id,
            'title'      => $request->title,
            'slug'       => $slug,
            'featured'   => isset($request->featured) == true ? $request->featured : '0',
            'content'    => $request->content,
        ]);

        //// Modification ////
        $flatDetails             = new FlatDetailsModel();
        $flatDetails->flat_dt_id = $chapter->id;
        $flatDetails->flat_title = $chapter->title;
        $flatDetails->flat_head  = "topic";
        $flatDetails->flat_value = $chapter->slug;
        $flatDetails->save();
        //// End Of Modification ////

        if($chapter)
        {
            return redirect()->route('list-topic')->with('success', 'Data added successfully');
        }
        else
        {
            return redirect()->route('list-topic')->with('error', 'Error in data adding!!!');
        }
    }

    public function edit($id)
    {
        $topic = Topic::find($id);

        $subjects = DB::table('subjects')
                        ->select('id', 'title','featured')
                        ->where('isActive', '1')
                        ->orderBy('title')
                        ->get();

        $chapters = DB::table('chapters')
                        ->select('id', 'title')
                        ->where('subject_id', $topic->subject_id)
                        ->where('isActive', '1')
                        ->orderBy('title')
                        ->get();

        return view('topics/edit', ['topic' => $topic, 'subjects'=>$subjects, 'chapters' => $chapters]);
    }

    public function update(Request $request, $id)
    {
        $topic = Topic::find($id);
        $slug = "";

        if($topic->title == $request->title)
        {
            $slug = $topic->slug;
        }
        else
        {
            $slug = SlugService::createSlug(Topic::class, 'slug', $request->title);
        }

        $topic->subject_id = $request->subject_id;
        $topic->chapter_id = $request->chapter_id;
        $topic->title      = $request->title;
        $topic->slug       = $slug;
        $topic->featured   = isset($request->featured) == true ? $request->featured : '0';
        $topic->content    = $request->content;

        //// Modification ////
        $flatDetails             = FlatDetailsModel::where('flat_dt_id', $id)->first();
        $flatDetails->flat_dt_id = $topic->id;
        $flatDetails->flat_title = $topic->title;
        $flatDetails->flat_head  = "topic";
        $flatDetails->flat_value = $topic->slug;
        $flatDetails->save();
        //// End Of Modification ////

        if($topic->save())
        {
            return redirect()->route('list-topic')->with('success', 'Data updated successfully');
        }
        else{
            return redirect()->route('list-topic')->with('error', 'Data not updated!!!');
        }
    }

    public function statusChange($id, $status)
    {
        $result = DB::update('update topics set isActive = ? where id = ?', [$status, $id]);

        if($result)
        {
            return redirect()->route('list-topic')->with('success', 'Record status changed successfully!!!');
        }
        else
        {
            return redirect()->route('list-topic')->with('error', 'Record status not changed!!!');
        }
    }

    public function delete($id)
    {
        $topic = Topic::find($id);

        FlatDetailsModel::where('id', $id)->delete();

        if($topic->delete($id))
        {
            return redirect()->route('list-topic')->with('success', 'Record deleted successfully!!!');
        }
        else{
            return redirect()->route('list-topic')->with('error', 'Record not deleted!!!');
        }
    }
}

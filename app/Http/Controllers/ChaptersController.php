<?php

namespace App\Http\Controllers;

use App\Chapter;
use App\FlatDetailsModel;
use App\Subject;
use Illuminate\Http\Request;
use Cviebrock\EloquentSluggable\Services\SlugService;
use DB;

class ChaptersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $chapters = Chapter::select('id', 'subject_id', 'title', 'isActive', 'created_at')
            ->orderBy('created_at', 'desc')
            ->with('subject')->get();

        //dd($chapters);
        return view('chapters/index', ['chapters' => $chapters]);
    }

    public function add()
    {
        $subjects = DB::table('subjects')
                        ->select('id', 'title')
                        ->where('isActive', '1')
                        ->orderBy('title')
                        ->get();

        return view('chapters/add', ['subjects'=>$subjects]);
    }

    public function createChapter(Request $request)
    {
        $slug = SlugService::createSlug(Chapter::class, 'slug', $request->title);
        //dd($slug);
        $chapter = Chapter::create([
            'subject_id' => $request->subject_id,
            'title'      => $request->title,
            'slug'       => $slug,
            'featured'   => isset($request->featured) == true ? $request->featured : '0',
            'content'    => $request->content,
        ]);

        //// Modification ////
        $flatDetails             = new FlatDetailsModel();
        $flatDetails->flat_dt_id = $chapter->id;
        $flatDetails->flat_title = $chapter->title;
        $flatDetails->flat_head  = "chapter";
        $flatDetails->flat_value = $chapter->slug;
        $flatDetails->save();
        //// End Of Modification ////

        if($chapter)
        {
            return redirect()->route('list-chapter')->with('success', 'Data added successfully');
        }
        else
        {
            return redirect()->route('list-chapter')->with('error', 'Error in data adding!!!');
        }
    }

    public function edit($id)
    {
        $subjects = DB::table('subjects')
                        ->select('id', 'title','featured')
                        ->where('isActive', '1')
                        ->orderBy('title')
                        ->get();

        $chapter = Chapter::find($id);
        return view('chapters/edit', ['chapter' => $chapter, 'subjects'=>$subjects]);
    }

    public function update(Request $request, $id)
    {
        $chapter = Chapter::find($id);
        $chapter->subject_id = $request->subject_id;
        $chapter->title = $request->title;
        $slug = "";

        if($chapter->title == $request->title)
        {
            $slug = $chapter->slug;
        }
        else
        {
            $slug = SlugService::createSlug(Chapter::class, 'slug', $request->title);
        }

        $chapter->slug = $slug;
        $chapter->featured = isset($request->featured) == true ? $request->featured : '0';
        $chapter->content = $request->content;

        //// Modification ////
        $flatDetails             = FlatDetailsModel::where('flat_dt_id', $id)->first();
        $flatDetails->flat_dt_id = $chapter->id;
        $flatDetails->flat_title = $chapter->title;
        $flatDetails->flat_head  = "chapter";
        $flatDetails->flat_value = $chapter->slug;
        $flatDetails->save();
        //// End Of Modification ////

        if($chapter->save())
        {
            return redirect()->route('list-chapter')->with('success', 'Data updated successfully');
        }
        else{
            return redirect()->route('list-chapter')->with('error', 'Data not updated!!!');
        }
    }

    public function statusChange($id, $status)
    {
        $result = DB::update('update chapters set isActive = ? where id = ?', [$status, $id]);
        //dd($result);
        if($result)
        {
            return redirect()->route('list-chapter')->with('success', 'Record status changed successfully!!!');
        }
        else
        {
            return redirect()->route('list-chapter')->with('error', 'Record status not changed!!!');
        }
    }

    public function delete($id)
    {
        $chapter = Chapter::find($id);

        FlatDetailsModel::where('id', $id)->delete();

        if($chapter->delete($id))
        {
            return redirect()->route('list-chapter')->with('success', 'Record deleted successfully!!!');
        }
        else{
            return redirect()->route('list-chapter')->with('error', 'Record not deleted!!!');
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Faq;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Cviebrock\EloquentSluggable\Services\SlugService;

class FaqController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showfaq()
    {
        $subject = DB::table('subjects')
            ->select('id', 'title', 'slug','featured')
            ->where('isActive', 1)
            ->get();

        return view('showfaq', compact('subject'));
    }

    public function index() {

        $fetchdata = Faq::select('id', 'title', 'isActive', 'created_at')
            ->orderBy('created_at', 'desc')
            ->get();

        return view('faq.view', compact('fetchdata'));
    }

    public function add()
    {
        return view('faq.add');
    }

    public function createFaq(Request $request)
    {
        $slug = SlugService::createSlug(Faq::class, 'slug', $request->title);

        $faqs = Faq::create([
            'title'    => $request->title,
            'slug'       => $slug,
            'content'  => $request->content,
        ]);

        if($faqs)
        {
            return redirect()->route('list-faq')->with('success', 'Data added successfully');
        }
        else{
            return "Faq not created!!!";
        }
    }

    public function edit($id)
    {
        $faq = Faq::find($id);

        return view('faq.edit', compact('faq'));
    }

    public function update(Request $request, $id)
    {
        $faq = Faq::find($id);
        $faq->title    = $request->title;
        $slug="";
        if($faq->title == $request->title)
        {
            $slug = $faq->slug;
        }
        else
        {
            $slug = SlugService::createSlug(Faq::class, 'slug', $request->title);
        }
        $faq->content  = $request->content;

        if($faq->save())
        {
            return redirect()->route('list-faq')->with('success', 'Data updated successfully');
        }
        else{
            return redirect()->route('list-faq')->with('error', 'Data not updated!!!');
        }
    }

    public function statusChange($id, $status)
    {
        $result = DB::update('update faqs set isActive = ? where id = ?', [$status, $id]);
        //dd($result);
        if($result)
        {
            return redirect()->route('list-faq')->with('success', 'Record status changed successfully!!!');
        }
        else
        {
            return redirect()->route('list-faq')->with('error', 'Record status not changed!!!');
        }
    }

    public function delete($id)
    {
        $faq = Faq::find($id);

        if($faq->delete($id))
        {
            return redirect()->route('list-faq')->with('success', 'Record deleted successfully!!!');
        }
        else{
            return redirect()->route('list-faq')->with('error', 'Record not deleted!!!');
        }
    }
}


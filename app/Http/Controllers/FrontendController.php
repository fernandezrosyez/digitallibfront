<?php

namespace App\Http\Controllers;

use App\Chapter;
use App\Subject;
use App\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FrontendController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index_BKP($id=null) {

        if($id!=null) {
            $defaultSubjectId    = $id;
            $getDefaultSubjectId = DB::table('subjects')
                ->select('id', 'title','featured')
                ->where('id', $defaultSubjectId)
                ->orderBy('id','ASC')
                ->first();
        } else {
            $getDefaultSubjectId = DB::table('subjects')
                ->select('id', 'title','featured')
                ->where('isActive', '1')
                ->orderBy('id','ASC')
                ->first();
            if(!empty($getDefaultSubjectId))
            {
                $defaultSubjectId = $getDefaultSubjectId->id;
            }
            else
            {
                $defaultSubjectId = 0;
            }
        }

        $chapterArray = array();
        $subjectArray = array();
        $topicArray   = array();

        if($defaultSubjectId>0) {
            $subjectArray[$defaultSubjectId]['subject_name'] = $getDefaultSubjectId->title;
            $subjectArray[$defaultSubjectId]['subject_id']   = $getDefaultSubjectId->id;
            $getDefaultSubjectRecord = DB::table('chapters')
                ->select('*')
                ->where('subject_id',$defaultSubjectId)
                ->orderBy('id','ASC')
                ->get();
            //dd($getDefaultSubjectRecord);
            if($getDefaultSubjectRecord) {
                $i = 0;
                foreach($getDefaultSubjectRecord as $resultSubjectChapter) {
                    $chapterId                         = $resultSubjectChapter->id;
                    $chapterArray[$i]['id']            = $chapterId;
                    $chapterArray[$i]['chapters_name'] = $resultSubjectChapter->title;
                    $getTopicRecod                     = DB::table('topics')
                                                            ->select('*')
                                                            ->where('chapter_id', $chapterId)
                                                            ->get();
                    //dd($getTopicRecod);
                    //echo $chapterId;
                    if($getTopicRecod) {
                        $j  =   0;
                        $topicArray            =   array();
                        foreach($getTopicRecod as $resultTopicRecord) {
                            $topicArray[$j]['id']          = $resultTopicRecord->id;
                            $topicArray[$j]['chapter_id']  = $resultTopicRecord->chapter_id;
                            $topicArray[$j]['topic_name']  = $resultTopicRecord->title;
                            $j++;
                            $chapterArray[$i]['topics']    = $topicArray;
                        }
                    }
                    $i++;
                }
                //echo "<pre>";
                //print_r($chapterArray);
            }
            $subjectArray[$defaultSubjectId]['chapters'] = $chapterArray;
        }
        return view('welcome',['subjectArray' => $subjectArray]);
    }


    public function chapterlistBKP($subject_id=null, $chapter_id=null, $topic_id=null) {

        $getSubject      = DB::table('subjects')
                            ->select('id', 'title','featured')
                            ->where('id', $subject_id)
                            ->first();

        $get_subject_id  = $getSubject->id;

        $getChapter      = DB::table('chapters')
                            ->select('*')
                            ->where('id', $chapter_id)
                            ->first();

        $get_chapter_id  = $getChapter->id;
        $chapter_name    = $getChapter->title;
        $getAllTopics    = DB::table('topics')
                            ->select('*')
                            ->where('chapter_id', $chapter_id)
                            ->orderBy('id','ASC')
                            ->get();

        if($topic_id===null) {
            $getTopics   = DB::table('topics')
                            ->select('*')
                            ->where('chapter_id', $chapter_id)
                            ->orderBy('id','ASC')
                            ->first();
        } else {
            $getTopics   = DB::table('topics')
                            ->select('*')
                            ->where('id', $topic_id)
                            ->first();
        }

        $chapter_name    = $getChapter->title;
        $topicMenuArray  = array();
        $topicsArray     = array();
        //exit;

        if($getTopics) {
            $topicMenuArray['subject_id']    = $get_subject_id;
            $topicMenuArray['chapter_id']    = $get_chapter_id;
            $topicMenuArray['chapter_name']  = $chapter_name;

            $topicsArray['chapter_name']     = $chapter_name;
            $topicsArray['topic_name']       = $getTopics->title;
            $topicsArray['topic_content']    = $getTopics->content;
            //$getTopicsArray                  = $getTopics->toArray;
            //dd($getAllTopics);
            foreach($getAllTopics as $resultTopics) {
                $topicMenuArray['topics'][$resultTopics->id] = $resultTopics->title;
            }
        }

        //Updated By Subrata Saha
        $subject = DB::table('subjects')
            ->select('id', 'title', 'slug','featured')
            ->where('isActive', 1)
            ->where('featured', 1)
            ->get();
        //End Of Updation
        //echo("<pre>");
        //print_r($topicMenuArray);
        //exit;
        //return view('chapterlisting',['topicMenuArray' => $topicMenuArray, 'topicsArray' => $topicsArray]);
        return view('innerpage', compact('topicMenuArray', 'topicsArray', 'subject'));
    }


    //Modified By Subrata Saha//
    public function chapterlist($slug=null, $chapSlug=null, $topicSlug=null) {

        $getSubject = DB::table('subjects')
            ->select('id', 'title', 'slug','featured')
            ->where('slug', $slug)
            ->first();

        $get_subject_id = $getSubject->id;

        $getChapter = DB::table('chapters')
            ->select('*')
            ->where('slug',$chapSlug)
            ->where('subject_id',$get_subject_id)
            ->first();
            $getChapterOthers = DB::table('chapters')
            ->select('*')
            ->where('slug','<>',$chapSlug)
            ->where('subject_id',$get_subject_id)
            ->get()->toArray();
           // print_r($getChapterOthers); exit;

        $get_chapter_id = $getChapter->id;
        $chapter_name   = $getChapter->title;

        $getAllTopics   = DB::table('topics')
            ->select('*')
            ->where('chapter_id', $get_chapter_id)
            ->orderBy('id','ASC')
            ->get();

        if($topicSlug===null) {
            $getTopics = DB::table('topics')
                ->select('*')
                ->where('chapter_id',$get_chapter_id)
                ->orderBy('id','ASC')
                ->first();
        } else {
            $getTopics = DB::table('topics')
                ->select('*')
                ->where('chapter_id',$get_chapter_id)
                ->where('slug', $topicSlug)
                ->first();
        }

        $chapter_name   = $getChapter->title;
        $topicMenuArray = array();
        $topicsArray    = array();

        if($getTopics) {
            $topicMenuArray['subject_id']   = $get_subject_id;
            $topicMenuArray['subject_slug'] = $getSubject->slug;
            $topicMenuArray['chapter_id']   = $get_chapter_id;
            $topicMenuArray['chapter_slug'] = $getChapter->slug;
            $topicMenuArray['chapter_name'] = $chapter_name;

            $topicsArray['chapter_name']    = $chapter_name;
            $topicsArray['topic_name']      = $getTopics->title;
            $topicsArray['topic_content']   = $getTopics->content;

            foreach($getAllTopics as $resultTopics) {
                $topicMenuArray['topics'][$resultTopics->slug] = $resultTopics->title;
            }
        }

        //Updated By Subrata Saha//
        $subject = DB::table('subjects')
            ->select('id', 'title', 'slug')
            ->where('isActive', 1)
            ->where('featured', 1)
            ->get();
        //End Of Updation//

        return view('innerpage', compact('topicMenuArray', 'topicsArray', 'subject','getChapterOthers'));
    }


    public function index() {

        $subject = DB::table('subjects')
            ->select('id', 'title', 'slug','featured')
            ->where('isActive', 1)
            //->where('featured', 1)
            ->get();

        // $chapter = DB::table('topics')
        //     ->select('topics.id', 'topics.subject_id as SubjectID', 'topics.chapter_id as ChapterID',
        //         'topics.title', 'topics.slug', 'subjects.title as SubjectTitle', 'subjects.slug as SubSlug',
        //         'chapters.title as ChapterTitle', 'chapters.slug as ChaptSlug')
        //     ->join('subjects','subjects.id','topics.subject_id')
        //     ->join('chapters','chapters.id','topics.chapter_id')
        //     ->where('topics.isActive', 1)
        //     //->distinct('subject_id', 'chapter_id')
        //     ->get();

            //    $chapter = DB::raw("select topics.chapter_id as ChapterID, topics.id, topics.subject_id as SubjectID, 
            //    topics.title, topics.slug,subjects.title as SubjectTitle,subjects.slug as SubSlug, chapters.title as ChapterTitle,
            //    chapters.slug as ChaptSlug from topics inner join subjects on subjects.id = topics.subject_id 
            //    inner join chapters on chapters.id = topics.chapter_id where topics.isActive = 1 group by topics.chapter_id")
            //   ->get();

            $chapter = DB::table('chapters')->where('chapters.isActive', 1)
              ->get();

        //dd($chapter);
        return view('index_page', compact('chapter', 'subject'));
    }

    public function Subjectindex($slug) {

        $subject = DB::table('subjects')
        ->select('id', 'title', 'slug','featured')
        ->where('isActive', 1)
        ->where('featured', 1)
        ->get();

    $chapter = DB::table('topics')
        ->select('topics.id', 'topics.subject_id as SubjectID', 'topics.chapter_id as ChapterID',
            'topics.title', 'topics.slug', 'subjects.title as SubjectTitle', 'subjects.slug as SubSlug',
            'chapters.title as ChapterTitle', 'chapters.slug as ChaptSlug')
        ->join('subjects','subjects.id','topics.subject_id')
        ->join('chapters','chapters.id','topics.chapter_id')
        ->where('topics.isActive', 1)
        ->where('subjects.slug', $slug)
        //->distinct('subject_id', 'chapter_id')
        ->get();//exit;

        $subjectDtls = DB::table('subjects')
            ->select('*')
            ->where('slug', $slug)
            ->first();

        return view('subjectpage', compact('subjectDtls', 'chapter', 'subject'));
    }

    public function example() {

        $expdata = array();

        $subject = DB::table('subjects')
            ->select('id', 'title', 'slug','featured')
            ->where('isActive', 1)
            ->where('featured', 1)
            ->get();

        $chapter = DB::table('topics')
            ->select('topics.id', 'topics.subject_id as SubjectID', 'topics.chapter_id as ChapterID',
                'topics.title', 'topics.slug', 'subjects.title as SubjectTitle', 'subjects.slug as SubSlug',
                'chapters.title as ChapterTitle', 'chapters.slug as ChaptSlug')
            ->join('subjects','subjects.id','topics.subject_id')
            ->join('chapters','chapters.id','topics.chapter_id')
            ->where('topics.isActive', 1)
            //->distinct('subject_id', 'chapter_id')
            ->get();

            $examples = DB::table('examples')
                        ->select('examples.id', 'examples.slug', 'subjects.title as subject_title', 'examples.title as title')
                        ->leftjoin('subjects', 'subjects.id', 'examples.subject_id')
                        ->where('examples.isActive', '1')
                        ->orderBy('examples.created_at', 'ASC')
                        ->get();

            if(!empty($examples))
            {
                foreach($examples as $example)
                {
                    $expdata[$example->subject_title][] = array(
                                                        'id'    => $example->id,
                                                        'slug' => $example->slug,
                                                        'title' => $example->title
                                                            );
                }
            }

            //dd($data);






        return view('example-page', compact('subject', 'chapter', 'expdata'));
    }

    public function exampleDetails($slug) {

        $subject = DB::table('subjects')
            ->select('id', 'title', 'slug','featured')
            ->where('isActive', 1)
            ->where('featured', 1)
            ->get();

        $chapter = DB::table('topics')
            ->select('topics.id', 'topics.subject_id as SubjectID', 'topics.chapter_id as ChapterID',
                'topics.title', 'topics.slug', 'subjects.title as SubjectTitle', 'subjects.slug as SubSlug',
                'chapters.title as ChapterTitle', 'chapters.slug as ChaptSlug')
            ->join('subjects','subjects.id','topics.subject_id')
            ->join('chapters','chapters.id','topics.chapter_id')
            ->where('topics.isActive', 1)
            //->distinct('subject_id', 'chapter_id')
            ->get();

            $example = DB::table('examples')
                        ->select('examples.id as exp_id', 'examples.slug', 'subjects.title as subject_title', 'examples.title as title', 'examples.content')
                        ->leftjoin('subjects', 'subjects.id', 'examples.subject_id')
                        ->where('examples.slug', $slug)
                        ->where('examples.isActive', '1')
                        ->orderBy('examples.created_at', 'ASC')
                        ->first();
            //dd($examples);

        return view('example-details-page', compact('subject', 'chapter', 'example'));
    }

    public function faq() {

        $expdata = array();

        $subject = DB::table('subjects')
            ->select('id', 'title', 'slug','featured')
            ->where('isActive', 1)
            ->where('featured', 1)
            ->get();

        $chapter = DB::table('topics')
            ->select('topics.id', 'topics.subject_id as SubjectID', 'topics.chapter_id as ChapterID',
                'topics.title', 'topics.slug', 'subjects.title as SubjectTitle', 'subjects.slug as SubSlug',
                'chapters.title as ChapterTitle', 'chapters.slug as ChaptSlug')
            ->join('subjects','subjects.id','topics.subject_id')
            ->join('chapters','chapters.id','topics.chapter_id')
            ->where('topics.isActive', 1)
            //->distinct('subject_id', 'chapter_id')
            ->get();

            $faqs = DB::table('faqs')
                        ->select('faqs.id', 'faqs.slug', 'faqs.title as title')
                        ->where('faqs.isActive', '1')
                        ->orderBy('faqs.created_at', 'ASC')
                        ->get();


        return view('faq-page', compact('subject', 'chapter', 'faqs'));
    }

    public function faqDetails($slug) {

        $subject = DB::table('subjects')
        ->select('id', 'title', 'slug','featured')
        ->where('isActive', 1)
        ->where('featured', 1)
        ->get();

    $chapter = DB::table('topics')
        ->select('topics.id', 'topics.subject_id as SubjectID', 'topics.chapter_id as ChapterID',
            'topics.title', 'topics.slug', 'subjects.title as SubjectTitle', 'subjects.slug as SubSlug',
            'chapters.title as ChapterTitle', 'chapters.slug as ChaptSlug')
        ->join('subjects','subjects.id','topics.subject_id')
        ->join('chapters','chapters.id','topics.chapter_id')
        ->where('topics.isActive', 1)
        //->distinct('subject_id', 'chapter_id')
        ->get();

        $faqs = DB::table('faqs')
                        ->select('faqs.id', 'faqs.slug', 'faqs.title as title', 'faqs.content')
                        ->where('faqs.isActive', '1')
                        ->where('faqs.slug', $slug)
                        ->orderBy('faqs.created_at', 'ASC')
                        ->first();

        return view('faq-details-page', compact('subject', 'chapter', 'faqs'));
    }
}

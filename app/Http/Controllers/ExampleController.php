<?php

namespace App\Http\Controllers;

use App\Example;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Cviebrock\EloquentSluggable\Services\SlugService;

class ExampleController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {

        $fetchdata = Example::select('id', 'subject_id', 'title', 'isActive', 'created_at')
            ->orderBy('created_at', 'desc')
            ->with('subject')->get();

        return view('example.view', compact('fetchdata'));
    }

    public function add() {

        $subjects = DB::table('subjects')
            ->select('id', 'title','featured')
            ->where('isActive', '1')
            ->orderBy('title')
            ->get();

        return view('example.add', compact('subjects'));
    }

    public function getAllChaptersBKP(Request $request) {

        $subject_id = $request->sub_id;
        //dd($subject_id);
        $chapters = DB::table('chapters')
            ->select('id', 'title')
            ->where('isActive', '1')
            ->where('subject_id', $subject_id)
            ->orderBy('title')
            ->get();
        //dd($chapters);
        return response()->json($chapters);
    }

    public function getAllChapters(Request $request) {

        /*$subject_id = $request->sub_id;
        //dd($subject_id);
        $chapters = DB::table('chapters')
            ->select('id', 'title')
            ->where('isActive', '1')
            ->where('subject_id', $subject_id)
            ->orderBy('title')
            ->get();
        //dd($chapters);
        return response()->json($chapters);*/

        $chapter = DB::table('chapters')
            ->where('isActive', '1')
            ->where('subject_id', $request->id)
            ->get();
        dd($chapter);
        $html = '<option value="">Select Chapter</option>';

        foreach ($chapter as $sub) {
            $html .= '<option value="' . $sub->id . '">' . $sub->title . '</option>';
        }

        return $html;
    }

    public function getAllTopics(Request $request) {

        $topic = DB::table('topics')
            ->where('isActive', '1')
            ->where('subject_id', $request->id)
            ->where('chapter_id', $request->chapter_id)
            ->get();
        dd($topic);
        $html = '<option value="">Select Topic</option>';

        foreach ($topic as $sub) {
            $html .= '<option value="' . $sub->id . '">' . $sub->title . '</option>';
        }

        return $html;
    }

    public function createExample(Request $request)
    {
        $slug = SlugService::createSlug(Example::class, 'slug', $request->title);

        $example = Example::create([
            'subject_id' => $request->subject_id,
            'title'    => $request->title,
            'slug'       => $slug,
            'content'  => $request->content,
        ]);


        if($example)
        {
            return redirect()->route('list-example')->with('success', 'Data added successfully');
        }
        else{
            return "Subject not created!!!";
        }
    }

    public function edit($id)
    {
        $example = Example::find($id);

        $subjects = DB::table('subjects')
        ->select('id', 'title','featured')
        ->where('isActive', '1')
        ->orderBy('title')
        ->get();

        return view('example.edit', compact('example', 'subjects'));
    }

    public function update(Request $request, $id)
    {
        $example = Example::find($id);
        $example->subject_id    = $request->subject_id;
        $example->title    = $request->title;
        $slug="";
        if($example->title == $request->title)
        {
            $slug = $example->slug;
        }
        else
        {
            $slug = SlugService::createSlug(Example::class, 'slug', $request->title);
        }
        $example->content  = $request->content;

        if($example->save())
        {
            return redirect()->route('list-example')->with('success', 'Data updated successfully');
        }
        else{
            return redirect()->route('list-example')->with('error', 'Data not updated!!!');
        }
    }

    public function statusChange($id, $status)
    {
        $result = DB::update('update examples set isActive = ? where id = ?', [$status, $id]);
        //dd($result);
        if($result)
        {
            return redirect()->route('list-example')->with('success', 'Record status changed successfully!!!');
        }
        else
        {
            return redirect()->route('list-example')->with('error', 'Record status not changed!!!');
        }
    }

    public function delete($id)
    {
        $subject = Example::find($id);

        if($subject->delete($id))
        {
            return redirect()->route('list-example')->with('success', 'Record deleted successfully!!!');
        }
        else{
            return redirect()->route('list-example')->with('error', 'Record not deleted!!!');
        }
    }
}

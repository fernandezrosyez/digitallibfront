<?php

namespace App\Http\Controllers;
use App\FlatDetailsModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Cviebrock\EloquentSluggable\Services\SlugService;
use App\Subject;
use DB;

class SubjectsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $subjects = DB::select('select id, title, isActive, created_at, updated_at from subjects');
        //dd($subjects);
        return view('subject/index', ['subjects'=>$subjects]);
    }

    public function add()
    {
        return view('subject/add');
    }

    public function createSubject(Request $request)
    {
        $slug = SlugService::createSlug(Subject::class, 'slug', $request->title);

        $subject = Subject::create([
            'title'    => $request->title,
            'slug'     => $slug,
            'featured' => isset($request->featured) == true ? $request->featured : '0',
            'content'  => $request->content,
        ]);

        //// Modification ////
        $flatDetails             = new FlatDetailsModel();
        $flatDetails->flat_dt_id = $subject->id;
        $flatDetails->flat_title = $subject->title;
        $flatDetails->flat_head  = "subject";
        $flatDetails->flat_value = $subject->slug;
        $flatDetails->save();
        //// End Of Modification ////

        if($subject)
        {
            return redirect()->route('list-subject')->with('success', 'Data added successfully');
        }
        else{
            return "Subject not created!!!";
        }
    }

    public function edit($id)
    {
        $subject = Subject::find($id);
        return view('subject/edit', ['subject' => $subject]);
    }

    public function update(Request $request, $id)
    {
        $subject = Subject::find($id);
        $slug = "";
        if($subject->title == $request->title)
        {
            $slug = $subject->slug;
        }
        else
        {
            $slug = SlugService::createSlug(Subject::class, 'slug', $request->title);
        }

        $subject->title    = $request->title;
        $subject->slug     = $slug;
        $subject->featured = isset($request->featured) == true ? $request->featured : '0';
        $subject->content  = $request->content;

        //// Modification ////
        $flatDetails             = FlatDetailsModel::where('flat_dt_id', $id)->first();
        $flatDetails->flat_dt_id = $subject->id;
        $flatDetails->flat_title = $subject->title;
        $flatDetails->flat_head  = "subject";
        $flatDetails->flat_value = $subject->slug;
        $flatDetails->save();
        //// End Of Modification ////

        if($subject->save())
        {
            return redirect()->route('list-subject')->with('success', 'Data updated successfully');
        }
        else{
            return redirect()->route('list-subject')->with('error', 'Data not updated!!!');
        }
    }

    public function statusChange($id, $status)
    {
        $result = DB::update('update subjects set isActive = ? where id = ?', [$status, $id]);
        //dd($result);
        if($result)
        {
            return redirect()->route('list-subject')->with('success', 'Record status changed successfully!!!');
        }
        else
        {
            return redirect()->route('list-subject')->with('error', 'Record status not changed!!!');
        }
    }

    public function delete($id)
    {
        $subject = Subject::find($id);

        FlatDetailsModel::where('id', $id)->delete();

        if($subject->delete($id))
        {
            return redirect()->route('list-subject')->with('success', 'Record deleted successfully!!!');
        }
        else{
            return redirect()->route('list-subject')->with('error', 'Record not deleted!!!');
        }
    }
}

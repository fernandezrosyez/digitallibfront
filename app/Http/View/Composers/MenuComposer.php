<?php

namespace App\Http\View\Composers;

use App\Models\Chapter;
use App\Models\Subject;
use App\Models\Topic;

use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class MenuComposer
{
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $Subjects    =   DB::table('subjects')
                                ->select('id', 'title','featured')
                                ->where('isActive', '1')
                                ->orderBy('id','ASC')
                                ->take(4)
                                ->get();
        $view->with('Subjects', $Subjects->toArray());
    }
}
?>

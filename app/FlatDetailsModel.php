<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlatDetailsModel extends Model
{
    protected $table   = "flat_details";
    public $timestamps = true;
}

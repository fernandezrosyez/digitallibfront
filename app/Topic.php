<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Topic extends Model
{
    public $timestamps = true;

    use Sluggable;

    protected $fillable = [
        'subject_id',
        'chapter_id',
        'title',
        'slug',
        'content'
       ];

       public function sluggable(): array
       {
           return [
               'slug' => [
                   'source' => 'title'
               ]
           ];
       }

    public function subject()
    {
        return $this->belongsTo('App\Subject', 'subject_id')->select(['id', 'title']);
    }

    public function chapter()
    {
        return $this->belongsTo('App\Chapter', 'chapter_id')->select(['id', 'title']);
    }
}

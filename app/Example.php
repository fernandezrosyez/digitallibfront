<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Example extends Model
{
    protected $table   = "examples";
    public $timestamps = true;

    use Sluggable;

    protected $fillable = [
        'subject_id',
        'title',
        'slug',
        'content'
       ];

       public function sluggable(): array
        {
            return [
                'slug' => [
                    'source' => 'title'
                ]
            ];
        }


       public function subject()
       {
           return $this->belongsTo('App\Subject', 'subject_id')->select(['id', 'title']);
       }


}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Faq extends Model
{
    protected $table   = "faqs";
    public $timestamps = true;

    use Sluggable;

    protected $fillable = [
        'title',
        'slug',
        'content'
       ];

       public function sluggable(): array
       {
           return [
               'slug' => [
                   'source' => 'title'
               ]
           ];
       }
}

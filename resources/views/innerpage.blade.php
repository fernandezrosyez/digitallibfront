@extends('layouts.front')
@section('title', 'Details Page')
@section('content')
<section>
    <?php
        if(!empty($topicMenuArray)) {
            $topicsMenuListing = $topicMenuArray['topics'];
            $subjectSlug       = $topicMenuArray['subject_slug'];
            $chapterSlug       = $topicMenuArray['chapter_slug'];
            $chapter_name      = $topicMenuArray['chapter_name'];
        }
    ?>
    <div class="wrapper" style="width:100%!important; min-width:100%!important;">
        <nav id="sidebar">
            <div class="sideBarNav">
                <h3>{{ $chapter_name ?? '' }}</h3>
                <hr>
                @if(!empty($topicsMenuListing))
                <ul class="ultra" style="list-style-type:none!important;">
                    @foreach($topicsMenuListing as $topic => $resultLeftnav)
                    <li class="listing"><a href="{{ url("{$subjectSlug}/{$chapterSlug}/{$topic}") }}">{{ $resultLeftnav }}</a></li>
                    @endforeach
                </ul>
                @endif
            </div>
            
            <div class="sideBarNav">
            @foreach($getChapterOthers as $data)
                <h4>
                     <a href="{{ url("{$subjectSlug}/{$data->slug}") }}">{{ $data->title }}</a>
                </h4>
            @endforeach
            </div>
           
        </nav>
        <!-- Page Content Holder -->
        <div id="content">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div>
                    <button type="button" id="sidebarCollapse" class="navbar-btn"> <span></span> <span></span> <span></span> </button>
                </div>
            </nav>
            <div class="conSecLeft">
                <div class="row clearfix">
                    <div class="col-lg-10 col-md-10 col-sm-7 col-xs-12 ">
                        <div class="text-center">
                            <a href="#"><img class="img-responsive" src="{{ asset('front-end/images/banner-top.jpg') }}"></a>
                        </div>
                        <div class="main_body_scroll">
                            <div class="row">

                                <div class="col-xs-12">
                                    <div class="white_box_border">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="main_headding">
                                                    <h2>{{ $topicsArray['chapter_name'] ?? '' }}</h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-md-12">{!! $topicsArray['topic_content'] ?? '' !!}</div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-5 col-xs-12">
                        <div class="text-center">
                            <a href="#"><img class="img-responsive" src="{{ asset('front-end/images/banner-right.jpg') }}"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

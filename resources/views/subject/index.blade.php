@extends('layouts.admin')

@section('content')

<section role="main" class="content-body">
    <header class="page-header">
        <h2>Subject List</h2>

        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="index.html">
                        <i class="fas fa-home"></i>
                    </a>
                </li>
                <li><span>Dashboard</span></li>
                <li><span>Subject</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="sidebar-right"></a>
        </div>
    </header>

    <!-- start: page -->
        <div class="row">
            <div class="col">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                        </div>

                        <h2 class="card-title">List Subjects</h2>
                    </header>
                    <div class="card-body">
                        @include('flash-message')
                        <table class="table table-bordered table-striped mb-0" id="datatable-tabletools">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Created Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($subjects as $subject)
                                <tr>
                                    <td>{{$subject->title}}</td>
                                <td>@php echo date('d-m-Y', strtotime($subject->created_at)); @endphp</td>
                                    <td>
                                        @if($subject->isActive == '1')
                                            <a href="{{ route('status-subject', ['id' => $subject->id, 'status' =>'0']) }}">
                                                <img src="{{asset('img/icons/active.png')}}" alt="active" /></a>
                                        @else
                                        <a href="{{ route('status-subject', ['id' => $subject->id, 'status' =>'1']) }}">
                                                <img src="{{asset('img/icons/deactive.png')}}" alt="inactive" /></a>
                                        @endif
                                        <a href="{{ route('edit-subject', ['id' => $subject->id]) }}">
                                            <img src="{{asset('img/icons/edit.png')}}" alt="edit" /></a>
                                        <a href="{{ route('delete-subject', ['id' => $subject->id]) }}" onclick="return confirm('Are you sure want to delete this record?')" >
                                            <img src="{{asset('img/icons/delete.png')}}" alt="delete" /></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    <!-- end: page -->
</section>

@endsection



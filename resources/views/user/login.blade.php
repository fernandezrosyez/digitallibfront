@extends('layouts.user')

@section('content')

<form method="POST" action="{{ route('login') }}">
    @csrf
    <div class="form-group mb-3">
        <label>Username</label>
        <div class="input-group">
            <input id="email" type="email" class="form-control form-control-lg @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            <span class="input-group-append">
                <span class="input-group-text">
                    <i class="fas fa-user"></i>
                </span>
            </span>
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
                    </span>
                    @enderror
        </div>
    </div>

    <div class="form-group mb-3">
        <div class="clearfix">
            <label class="float-left">Password</label>
            <a href="pages-recover-password.html" class="float-right">Lost Password?</a>
        </div>
        <div class="input-group">
            <input id="password" type="password" class="form-control form-control-lg @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
            <span class="input-group-append">
                <span class="input-group-text">
                    <i class="fas fa-lock"></i>
                </span>
            </span>
                @error('password')
                <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
                </span>
                @enderror
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 text-right">
            <button type="submit" class="btn btn-primary mt-2">
                {{ __('Login') }}
            </button>
        </div>
    </div>
</form>


@endsection

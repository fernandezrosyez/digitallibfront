<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <title>Tutorial :: @yield('title')</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{asset('front-end/css/bootstrap/bootstrap.css')}}">
  <link rel="stylesheet" href="{{asset('front-end/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('front-end/css/style2.css')}}">
  <link rel="stylesheet" href="{{asset('front-end/css/sidebar.css')}}">
  <link href="{{asset('front-end/plugins/font-awesome-4.7.0/css/font-awesome.css')}}" rel="stylesheet" type="text/css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="{{asset('front-end/js/bootstrap.min.js')}}"></script>


</head>
<body>
    @include('elements.front-end-header')
<section>
	<div class="wrapper">
        <!-- Sidebar Holder -->
        @include('elements.front-end-sidebar')
        <!-- Page Content Holder -->
        @yield('content')
    </div>
</section>
@include('elements.front-end-footer')
   <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $(this).toggleClass('active');
            });
        });
    </script>


</body>
</html>

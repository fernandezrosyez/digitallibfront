<!doctype html>
<html class="fixed">
	<head>
		<!-- Basic -->
		<meta charset="UTF-8">
		<title>University | Administrative Panel</title>
		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Porto Admin - Responsive HTML5 Template">
		<meta name="author" content="okler.net">
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<!-- Web Fonts  -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,800,900|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
        <link rel="stylesheet" href="{{asset('vendor/bootstrap/css/bootstrap.css')}}" type="text/css" />
		<link rel="stylesheet" href="{{asset('vendor/animate/animate.css')}}" type="text/css" />

		<link rel="stylesheet" href="{{asset('vendor/font-awesome/css/all.min.css')}}" />
		<link rel="stylesheet" href="{{asset('vendor/magnific-popup/magnific-popup.css')}}" />
		<link rel="stylesheet" href="{{asset('vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css')}}" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="{{asset('vendor/jquery-ui/jquery-ui.css')}}" />
		<link rel="stylesheet" href="{{asset('vendor/jquery-ui/jquery-ui.theme.css')}}" />
		{{-- <link rel="stylesheet" href="{{asset('vendor/bootstrap-multiselect/css/bootstrap-multiselect.css')}}" /> --}}
        <link rel="stylesheet" href="{{asset('vendor/morris/morris.css')}}" />
        <link rel="stylesheet" href="{{asset('vendor/select2/css/select2.css')}}" />
        <link rel="stylesheet" href="{{asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css')}}" />
		<!--(remove-empty-lines-end)-->
		<!-- Theme CSS -->
		<link rel="stylesheet" href="{{asset('css/theme.css')}}" />
		<!--(remove-empty-lines-end)-->
		<!-- Skin CSS -->
		<link rel="stylesheet" href="{{asset('css/skins/default.css')}}" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="{{asset('css/custom.css')}}">

		<!-- Head Libs -->
        <script src="{{asset('vendor/modernizr/modernizr.js')}}"></script>
	</head>
	<body>
		<section class="body">
			<!-- start: header -->
            @include('elements.header')
			<!-- end: header -->
			<div class="inner-wrapper">
                <!-- start: sidebar -->
                @include('elements.sidebar')
				<!-- end: sidebar -->
				@yield('content')
			</div>
			<aside id="sidebar-right" class="sidebar-right">
				<div class="nano">
					<div class="nano-content">
						<a href="#" class="mobile-close d-md-none">
							Collapse <i class="fas fa-chevron-right"></i>
						</a>
						<div class="sidebar-right-wrapper">
							<div class="sidebar-widget widget-calendar">
								<h6>Upcoming Tasks</h6>
								<div data-plugin-datepicker data-plugin-skin="dark"></div>
								<ul>
									<li>
										<time datetime="2017-04-19T00:00+00:00">04/19/2017</time>
										<span>Company Meeting</span>
									</li>
								</ul>
							</div>
							<div class="sidebar-widget widget-friends">
								<h6>Friends</h6>
								<ul>
									<li class="status-online">
										<figure class="profile-picture">
											<img src="img/!sample-user.jpg" alt="Joseph Doe" class="rounded-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
									<li class="status-online">
										<figure class="profile-picture">
											<img src="img/!sample-user.jpg" alt="Joseph Doe" class="rounded-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
									<li class="status-offline">
										<figure class="profile-picture">
											<img src="img/!sample-user.jpg" alt="Joseph Doe" class="rounded-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
									<li class="status-offline">
										<figure class="profile-picture">
											<img src="img/!sample-user.jpg" alt="Joseph Doe" class="rounded-circle">
										</figure>
										<div class="profile-info">
											<span class="name">Joseph Doe Junior</span>
											<span class="title">Hey, how are you?</span>
										</div>
									</li>
								</ul>
							</div>

						</div>
					</div>
				</div>
			</aside>
		</section>

		<!-- Vendor -->
        <script src="{{asset('vendor/jquery/jquery.js')}}"></script>
		<script src="{{asset('vendor/jquery-browser-mobile/jquery.browser.mobile.js')}}"></script>
		<script src="{{asset('vendor/popper/umd/popper.min.js')}}"></script>
		<script src="{{asset('vendor/bootstrap/js/bootstrap.js')}}"></script>
		<script src="{{asset('vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
		<script src="{{asset('vendor/common/common.js')}}"></script>
		<script src="{{asset('vendor/nanoscroller/nanoscroller.js')}}"></script>
		<script src="{{asset('vendor/magnific-popup/jquery.magnific-popup.js')}}"></script>
		<script src="{{asset('vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>

		<!-- Specific Page Vendor -->
		<script src="{{asset('vendor/jquery-ui/jquery-ui.js')}}"></script>
		<script src="{{asset('vendor/jqueryui-touch-punch/jquery.ui.touch-punch.js')}}"></script>
		<script src="{{asset('vendor/jquery-appear/jquery.appear.js')}}"></script>
		<script src="{{asset('vendor/bootstrap-multiselect/js/bootstrap-multiselect.js')}}"></script>
		<script src="{{asset('vendor/jquery.easy-pie-chart/jquery.easypiechart.js')}}"></script>
		<script src="{{asset('vendor/flot/jquery.flot.js')}}"></script>
		<script src="{{asset('vendor/flot.tooltip/jquery.flot.tooltip.js')}}"></script>
		<script src="{{asset('vendor/flot/jquery.flot.pie.js')}}"></script>
		<script src="{{asset('vendor/flot/jquery.flot.categories.js')}}"></script>
		<script src="{{asset('vendor/flot/jquery.flot.resize.js')}}"></script>
		<script src="{{asset('vendor/jquery-sparkline/jquery.sparkline.js')}}"></script>
		<script src="{{asset('vendor/raphael/raphael.js')}}"></script>
		<script src="{{asset('vendor/morris/morris.js')}}"></script>
		<script src="{{asset('vendor/gauge/gauge.js')}}"></script>
		<script src="{{asset('vendor/snap.svg/snap.svg.js')}}"></script>
        <script src="{{asset('vendor/liquid-meter/liquid.meter.js')}}"></script>
        <script src="{{asset('/vendor/select2/js/select2.js')}}"></script>
		<!--(remove-empty-lines-end)-->

		<!-- Theme Base, Components and Settings -->
		<script src="{{asset('js/theme.js')}}"></script>

		<!-- Theme Custom -->
		<script src="{{asset('js/custom.js')}}"></script>

		<!-- Theme Initialization Files -->
		<script src="{{asset('js/theme.init.js')}}"></script>

		<!-- Examples -->
        <script src="{{asset('js/examples/examples.dashboard.js')}}"></script>
        <?php
            $routes_arr = array('list-subject', 'list-chapter');

            if (in_array(\Route::current()->getName(), $routes_arr)) {
        ?>
            <script src="{{asset('/vendor/select2/js/select2.js')}}"></script>
            <script src="{{asset('/vendor/datatables/media/js/jquery.dataTables.min.js')}}"></script>
            <script src="{{asset('/vendor/datatables/media/js/dataTables.bootstrap4.min.js')}}"></script>
            <script src="{{asset('/vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js')}}"></script>
            <script src="{{asset('/vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js')}}"></script>
            <script src="{{asset('/vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js')}}"></script>
            <script src="{{asset('/vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js')}}"></script>
            <script src="{{asset('/vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js')}}"></script>
            <script src="{{asset('/vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
            <script src="{{asset('/vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js')}}"></script>
            <script src="{{asset('/js/examples/examples.datatables.default.js')}}"></script>
            <script src="{{asset('/js/examples/examples.datatables.row.with.details.js')}}"></script>
            <script src="{{asset('/js/examples/examples.datatables.tabletools.js')}}"></script>

        <?php
            }
            $routes_arr = array('add-subject', 'edit-subject');

            if (in_array(\Route::current()->getName(), $routes_arr)) {
        ?>
            <script type="text/javascript">
                $(document).ready(function () {
                    jQuery('#sub_add_featured').on('click', function() {
                        if(jQuery(this).prop('checked') === true || jQuery(this).is(":checked") === true)
                        {
                            jQuery(this).val('1');
                        }
                        else
                        {
                            jQuery(this).val('0');
                        }
                   });

                   jQuery('#sub_edit_featured').on('click', function() {
                        if(jQuery(this).prop('checked') === true || jQuery(this).is(":checked") === true)
                        {
                            jQuery(this).val('1');
                        }
                        else
                        {
                            jQuery(this).val('0');
                        }
                   });
                });
            </script>
            <?php } ?>
            <?php
                $routes_arr = array('add-chapter', 'edit-chapter');

                if (in_array(\Route::current()->getName(), $routes_arr)) {
            ?>
            <script type="text/javascript">
                $(document).ready(function () {
                    jQuery('#chp_add_featured').on('click', function() {
                        if(jQuery(this).prop('checked') === true || jQuery(this).is(":checked") === true)
                        {
                            jQuery(this).val('1');
                        }
                        else
                        {
                            jQuery(this).val('0');
                        }
                   });

                   jQuery('#chp_edit_featured').on('click', function() {
                        if(jQuery(this).prop('checked') === true || jQuery(this).is(":checked") === true)
                        {
                            jQuery(this).val('1');
                        }
                        else
                        {
                            jQuery(this).val('0');
                        }
                   });
                });
            </script>
            <?php } ?>
            <?php
                $routes_arr_topic = array('add-topic', 'edit-topic');
                if (in_array(\Route::current()->getName(), $routes_arr_topic)) {
            ?>
            <script type="text/javascript">
                $(document).ready(function () {

                    $('#subject_id').on('change', function() {
                        var sub_id = $(this).val();
                        var html_chapter = "";
                        $.ajax({
                            url: "{{ route('get-chapters') }}",
                            type: "GET",
                            data: { sub_id: sub_id },
                            success: function(response) {
                                if(response.length >= 1)
                                {
                                    //console.log(response);
                                    $('#chapter_id').find('option').remove();
                                    //$("#chapter_id").remove();
                                    var html_option = "";
                                    html_option += '<option value="">Select a Chapter</option>';

                                    for(var i=0; i<response.length; i++)
                                    {
                                        var id = response[i].id;
                                        var title = response[i].title;

                                        html_option += '<option value="'+id+'">'+title+'</option>';
                                    }
                                    $("#chapter_id").append(html_option);
                                }
                            }
                        });
                    });

                    jQuery('#top_add_featured').on('click', function() {
                        if(jQuery(this).prop('checked') === true || jQuery(this).is(":checked") === true)
                        {
                            jQuery(this).val('1');
                        }
                        else
                        {
                            jQuery(this).val('0');
                        }
                   });

                   jQuery('#top_edit_featured').on('click', function() {
                        if(jQuery(this).prop('checked') === true || jQuery(this).is(":checked") === true)
                        {
                            jQuery(this).val('1');
                        }
                        else
                        {
                            jQuery(this).val('0');
                        }
                   });
               });
            </script>
        <?php
            }
        ?>
	</body>
</html>

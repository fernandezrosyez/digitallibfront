@extends('layouts.admin')

@section('content')

    <section role="main" class="content-body card-margin">
        <header class="page-header">
            <h2>Faq</h2>
            <div class="right-wrapper text-right">
                <ol class="breadcrumbs">
                    <li>
                        <a href="#">
                            <i class="fas fa-home"></i>
                        </a>
                    </li>
                    <li><span>Dashboard</span></li>
                    <li><span>Faq</span></li>
                </ol>
                <a class="sidebar-right-toggle" data-open="sidebar-right"></a>
            </div>
        </header>
        <!-- start: page -->
        <div class="row">
            <div class="col-lg-12">
                <form id="form" Method="POST" action="{{ route('create-faq') }}" class="form-horizontal">
                    @csrf
                    <section class="card">
                        <header class="card-header">
                            <h2 class="card-title">Add Faq</h2>
                        </header>
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-sm-3 control-label text-sm-right pt-2">Title <span class="required">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" name="title" class="form-control" required/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 control-label text-sm-right pt-2">Content </label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <textarea class="ckeditor form-control" name="content"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="card-footer">
                            <div class="row justify-content-end">
                                <div class="col-sm-9">
                                    <button class="btn btn-primary">Submit</button>
                                    <a href="{{ route('list-example') }}"><button type="button" class="btn btn-default">Back</button></a>
                                </div>
                            </div>
                        </footer>
                    </section>
                </form>
            </div>
        </div>
        <!-- end: page -->
    </section>

    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    {{-- <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script> --}}
    <script type="text/javascript">
        $(document).ready(function () {

            $('.ckeditor').ckeditor();

            /*$('#subjectsID').on('change', function() {
                var sub_id = $(this).val();
                var html_chapter = "";
                alert(sub_id);
                $.ajax({
                    url: "{{ route('get-all-chapters') }}",
                    type: "GET",
                    data: { sub_id: sub_id },
                    success: function(response) {
                        if(response.length >= 1)
                        {
                            //console.log(response);
                            $('#chaptersID').find('option').remove();
                            var html_option = "";
                            html_option += '<option value="">Select Any Chapter</option>';

                            for(var i=0; i<response.length; i++)
                            {
                                var id    = response[i].id;
                                var title = response[i].title;
                                html_option += '<option value="'+id+'">'+title+'</option>';
                            }
                            $("#chaptersID").append(html_option);
                        }
                    }
                });
            });*/
        });


    </script>

@endsection

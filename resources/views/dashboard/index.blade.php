@extends('layouts.admin')

@section('content')

<section role="main" class="content-body">
    <header class="page-header">
        <h2>Default Layout</h2>

        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="index.html">
                        <i class="fas fa-home"></i>
                    </a>
                </li>
                <li><span>Layouts</span></li>
                <li><span>Default</span></li>
            </ol>

            <a class="sidebar-right-toggle"></a>
        </div>
    </header>

    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
                <h2 class="text-center"><strong><em>Welcome To Dash Board</em></strong></h2>
        </div>
    </div>


    <!-- end: page -->
</section>


@endsection

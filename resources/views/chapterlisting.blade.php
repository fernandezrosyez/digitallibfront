@extends('layouts.front')

@section('content')
<section>
<?php
    if(!empty($topicMenuArray)) {
        //print_r($subjectArray);
        $topicsMenuListing  =   $topicMenuArray['topics'];
        $subject_id         =   $topicMenuArray['subject_id'];
        $chapter_id         =   $topicMenuArray['chapter_id'];
        $chapter_name       =   $topicMenuArray['chapter_name'];

    }
?>
<div class="wrapper" style="width:100%!important; min-width:100%!important;">
    <nav id="sidebar">
            <div class="sideBarNav">
                <h3><?php echo($chapter_name);?></h3>
                <?php
                    if(!empty($topicsMenuListing)) {

                ?>
				<ul>
                    <?php
                        foreach($topicsMenuListing as $topic_id=>$resultLeftnav) {
                    ?>
                    <li><a href="<?php echo(url("{$subject_id}/{$chapter_id}/{$topic_id}"));?>"><?php echo($resultLeftnav);?></a></li>
                    <?php
                        }
                    ?>
                </ul>
                <?php
                    }
                ?>
			</div>
        </nav>
    <!-- Page Content Holder -->

    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div>

                <button type="button" id="sidebarCollapse" class="navbar-btn">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>


            </div>
        </nav>

        <div class="conSecLeft">
        <div class="row clearfix">
            <div class="col-lg-10 col-md-10 col-sm-7 col-xs-12 ">
                <?php echo($topicsArray['topic_content']); ?>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-5 col-xs-12">
                <div class="text-center">
                    <a href="#"><img class="img-responsive" src="{{ asset('front-end/images/banner-right.jpg') }}"></a>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection

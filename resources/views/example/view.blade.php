@extends('layouts.admin')

@section('content')
    <section role="main" class="content-body">
        <header class="page-header">
            <h2>Example List</h2>
            <div class="right-wrapper text-right">
                <ol class="breadcrumbs">
                    <li>
                        <a href="#">
                            <i class="fas fa-home"></i>
                        </a>
                    </li>
                    <li><span>Dashboard</span></li>
                    <li><span>Example</span></li>
                </ol>
                <a class="sidebar-right-toggle" data-open="sidebar-right"></a>
            </div>
        </header>
        <!-- start: page -->
        <div class="row">
            <div class="col">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                        </div>
                        <h2 class="card-title">List Examples</h2>
                    </header>
                    <div class="card-body">
                        @include('flash-message')
                        <table class="table table-bordered table-striped mb-0" id="datatable-tabletools">
                            <thead>
                            <tr>
                                <th>Subject Name</th>
                                <th>Title</th>
                                <th>Created Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($fetchdata as $data)
                                <tr>
                                    <td>{{ $data->subject->title }}</td>
                                    <td>{{ $data->title }}</td>
                                    <td>@php echo date('d-m-Y', strtotime($data->created_at)); @endphp</td>
                                    <td>
                                        @if($data->isActive == '1')
                                            <a href="{{ route('status-example', ['id' => $data->id, 'status' =>'0']) }}">
                                                <img src="{{asset('img/icons/active.png')}}" alt="active" /></a>
                                        @else
                                            <a href="{{ route('status-example', ['id' => $data->id, 'status' =>'1']) }}">
                                                <img src="{{asset('img/icons/deactive.png')}}" alt="inactive" /></a>
                                        @endif
                                        <a href="{{ route('edit-example', ['id' => $data->id]) }}">
                                            <img src="{{asset('img/icons/edit.png')}}" alt="edit" /></a>
                                        <a href="{{ route('delete-example', ['id' => $data->id]) }}" onclick="return confirm('Are you sure want to delete this record?')" >
                                            <img src="{{asset('img/icons/delete.png')}}" alt="delete" /></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>
        <!-- end: page -->
    </section>
@endsection

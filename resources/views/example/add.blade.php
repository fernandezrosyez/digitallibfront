@extends('layouts.admin')

@section('content')

    <section role="main" class="content-body card-margin">
        <header class="page-header">
            <h2>Example</h2>
            <div class="right-wrapper text-right">
                <ol class="breadcrumbs">
                    <li>
                        <a href="#">
                            <i class="fas fa-home"></i>
                        </a>
                    </li>
                    <li><span>Dashboard</span></li>
                    <li><span>Example</span></li>
                </ol>
                <a class="sidebar-right-toggle" data-open="sidebar-right"></a>
            </div>
        </header>
        <!-- start: page -->
        <div class="row">
            <div class="col-lg-12">
                <form id="form" Method="POST" action="{{ route('createExample') }}" class="form-horizontal">
                    @csrf
                    <section class="card">
                        <header class="card-header">
                            <h2 class="card-title">Add Example</h2>
                        </header>
                        <div class="card-body">

                            <div class="form-group row">
                                <label class="col-sm-3 control-label text-sm-right pt-2">Subject <span class="required">*</span></label>
                                <div class="col-sm-9">
                                    <select id="subject_id" name="subject_id" data-plugin-selectTwo class="form-control populate" title="Please select at least one Subject" required>
                                        <option value="">Choose Any Subject</option>
                                        @foreach($subjects as $subject)
                                            <option value="{{ $subject->id }}">{{ $subject->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 control-label text-sm-right pt-2">Title <span class="required">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" name="title" class="form-control" required/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 control-label text-sm-right pt-2">Content </label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <textarea class="ckeditor form-control" name="content"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="card-footer">
                            <div class="row justify-content-end">
                                <div class="col-sm-9">
                                    <button class="btn btn-primary">Submit</button>
                                    <a href="{{ route('list-example') }}"><button type="button" class="btn btn-default">Back</button></a>
                                </div>
                            </div>
                        </footer>
                    </section>
                </form>
            </div>
        </div>
        <!-- end: page -->
    </section>

    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    {{-- <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script> --}}
    <script type="text/javascript">
        $(document).ready(function () {

            $('.ckeditor').ckeditor();

            /*$('#subjectsID').on('change', function() {
                var sub_id = $(this).val();
                var html_chapter = "";
                alert(sub_id);
                $.ajax({
                    url: "{{ route('get-all-chapters') }}",
                    type: "GET",
                    data: { sub_id: sub_id },
                    success: function(response) {
                        if(response.length >= 1)
                        {
                            //console.log(response);
                            $('#chaptersID').find('option').remove();
                            var html_option = "";
                            html_option += '<option value="">Select Any Chapter</option>';

                            for(var i=0; i<response.length; i++)
                            {
                                var id    = response[i].id;
                                var title = response[i].title;
                                html_option += '<option value="'+id+'">'+title+'</option>';
                            }
                            $("#chaptersID").append(html_option);
                        }
                    }
                });
            });*/
        });

        $('#subject_id').on('change', function() {
            var subject_id = $('#subject_id').val();
            alert(subject_id);
            $.ajax({
                url: '{{ route('get-all-chapters') }}',
                type: "POST",
                data:{_token:'<?php echo csrf_token(); ?>',id:subject_id },
                success:function(data) {
                    $('#chapter_id').html(data);
                }
            });
        });

        $('#chapter_id').on('change', function() {
            var subject_id = $('#subject_id').val();
            var chapter_id = $('#chapter_id').val();
            alert(chapter_id);
            $.ajax({
                url: '{{ route('get-all-topics') }}',
                type: "POST",
                data:{_token:'<?php echo csrf_token(); ?>',id:chapter_id, subject_id:subject_id },
                success:function(data) {
                    $('#topic_id').html(data);
                }
            });
        });
    </script>

@endsection

<div class="header">
    <div class="wrapper clearfix">
        <button type="button" class="open-menu mobile-only"><i class="icon-menu"></i></button>
        <div class="logo">
            <a href="{{ route('index') }}"><img src="{{asset('img/logo.png')}}" alt="TutorialFiles" /></a>
        </div>
        <div class="site-search">
            <form action="https://www.google.com/search" method="get" target="_blank" class="clearfix">
                <input type="hidden" value="www.tutorialfiles.com" name="sitesearch" />
                <input type="text" name="q" placeholder="Search topics, tutorials, questions and answers..." id="searchInput" class="search" />
                <button type="submit" class="search-btn"><i class="fa fa-search" aria-hidden="true"></i></button>
            </form>
        </div>
        <div class="social" style="font-size: 27px;
    margin-inline-start: auto;">
            <a href="" target="_blank" title="Join us on Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            <a href="" target="_blank" title="Follow us on Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            <a href="" target="_blank" title="Send us an Email"><i class="fa fa-envelope" aria-hidden="true"></i></a>
        </div>
        <button type="button" class="open-search mobile-only">
            <i class="icon-search"></i>
        </button>
    </div>
</div>
<div class="menu">
    <div class="wrapper">
        <a href="{{ url('/') }}" title="Home Page">HOME</a>
        @if(!empty($subject))
            @foreach($subject as $data)
            @if($data->featured==1)
        <a href="{{ url("{$data->slug}") }}" title="{{ $data->title }}">{{ $data->title }}</a>
        @endif
            @endforeach
        @endif
        <a href="{{ url('example') }}" title="Example Page">Examples</a>
        <a href="{{ url('faq') }}" title="FAQ Page">Faq</a>
    </div>
</div>

<style>
.remove_bullets
{
    list-style-type: none!important;
}
@media screen and (max-width: 600px) {
	.remove_bullets{
    text-align: center;
  }
}
	</style>
<footer>
	<div class="footerTop">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					Is this website helpful to you? Please give us a <a href="#">like</a>, or share your <a href="#">feedback</a>. Connect with us on <a href="#">Facebook</a>, <a href="#">Twitter</a> and <a href="#">Google+</a> for the latest updates.
				</div>
			</div>
		</div>
	</div>

	<div class="footerMid">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
					<div>
						<img class="img-responsive" src="{{asset('img/logo.png')}}">
					</div>

				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
					<h4 class="remove_bullets">ABOUT US</h4>
					<ul>
						<li class="remove_bullets"><a href="#">Our Story</a></li>
						<li class="remove_bullets"><a href="#">Terms of Use</a></li>
						<li class="remove_bullets"><a href="#">Privacy Policy</a></li>
					</ul>

				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
					<h4 class="remove_bullets">CONTACT</h4>
					<ul >
						<li class="remove_bullets"><a href="#">Contact Us</a></li>
						<li class="remove_bullets"><a href="#">Report Error</a></li>
						<li class="remove_bullets"><a href="#">Advertise</a></li>
					</ul>

				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
					<h4 class="remove_bullets">INTERACTIVE TOOLS</h4>
					<ul>
						<li class="remove_bullets"><a href="#">Title &amp; Meta Length Calculator</a></li>
						<li class="remove_bullets"><a href="#">Bootstrap Button Generator</a></li>
						<li class="remove_bullets"><a href="#">SQL Playground</a></li>
					</ul>


				</div>

			</div>
		</div>
	</div>


	<div class="footerBotom">
		<div class="container">
			<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			Copyright © 2018  . All Rights Reserved.
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
			<div class="social-media">
					<em>Share This:</em>
                    <a href="#" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a href="#" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    <a href="#" title="Google+"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                    <a href="#" title="Gmail"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                </div>

		</div>
	</div>
		</div>
	</div>
</footer>

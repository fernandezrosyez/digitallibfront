<aside id="sidebar-left" class="sidebar-left">

    <div class="sidebar-header">
        <div class="sidebar-title">
            Navigation
        </div>
        <div class="sidebar-toggle d-none d-md-block" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">

                <ul class="nav nav-main">
                    <li>
                        <a class="nav-link" href="layouts-default.html">
                            <i class="fas fa-home" aria-hidden="true"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class="fa fa-file" aria-hidden="true"></i>
                            <span>Subject</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="{{ route('add-subject')}}">
                                    Add Subject
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{ route('list-subject') }}">
                                    Subject List
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class="fa fa-book" aria-hidden="true"></i>
                            <span>Chapter</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="{{ route('add-chapter')}}">
                                    Add Chapter
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{ route('list-chapter') }}">
                                    Chapter List
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class="fa fa-book" aria-hidden="true"></i>
                            <span>Topic</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="{{ route('add-topic')}}">
                                    Add Topic
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{ route('list-topic') }}">
                                    Topic List
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class="fa fa-file" aria-hidden="true"></i>
                            <span>Example</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="{{ route('add-example')}}">
                                    Add Example
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{ route('list-example') }}">
                                    Example List
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-parent">
                        <a class="nav-link" href="#">
                            <i class="fa fa-file" aria-hidden="true"></i>
                            <span>Faq</span>
                        </a>
                        <ul class="nav nav-children">
                            <li>
                                <a class="nav-link" href="{{ route('add-faq')}}">
                                    Add Faq
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" href="{{ route('list-faq') }}">
                                    Faq List
                                </a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </nav>
        </div>

        <script>
            // Maintain Scroll Position
            if (typeof localStorage !== 'undefined') {
                if (localStorage.getItem('sidebar-left-position') !== null) {
                    var initialPosition = localStorage.getItem('sidebar-left-position'),
                        sidebarLeft = document.querySelector('#sidebar-left .nano-content');

                    sidebarLeft.scrollTop = initialPosition;
                }
            }
        </script>


    </div>

</aside>

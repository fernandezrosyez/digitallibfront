@extends('layouts.front')
@section('title', 'Example Details')
@section('content')

    <section>
        <div class="wrapper" style="width:100%!important; min-width:100%!important;">
            <nav id="sidebar">
                @if(!empty($chapter))
                    @foreach($chapter as $data)
                <div class="sideBarNav">
                    <h3>{{ $data->SubjectTitle }}</h3>
                    <hr>
                    <ul class="ultra" style="list-style-type:none!important;">
                        <li class="listing">
                            <a href="{{ url("{$data->SubSlug}/{$data->ChaptSlug}") }}">{{ $data->ChapterTitle }}</a>
                        </li>
                    </ul>
                </div>
                    @endforeach
                @endif
            </nav>
            <!-- Page Content Holder -->
            <div id="content">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div>
                        <button type="button" id="sidebarCollapse" class="navbar-btn"> <span></span> <span></span> <span></span> </button>
                    </div>
                </nav>
                <div class="conSecLeft">
                    <div class="row clearfix">
                        <div class="col-lg-10 col-md-10 col-sm-7 col-xs-12 ">
                            <div class="text-center">
                                <a href="#"><img class="img-responsive" src="{{ asset('front-end/images/banner-top.jpg') }}"></a>
                            </div>
                            <div class="main_body_scroll">

                                <div class="containSection">
                                    <h1>{{ $example->subject_title }}</h1>
                                </div>
                                <div class="containSection">
                                    <div class="info-box">
                                        <h2>{{ $example->title }}</h2>
                                        <div class="part">
                                            {!! $example->content !!}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-5 col-xs-12">
                            <div class="text-center">
                                <a href="#"><img class="img-responsive" src="{{ asset('front-end/images/banner-right.jpg') }}"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="{{asset('front-end/prism.js')}}"></script>
@endsection

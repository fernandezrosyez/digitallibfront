@extends('layouts.admin')

@section('content')

<section role="main" class="content-body card-margin">
    <header class="page-header">
        <h2>Topic</h2>

        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="index.html">
                        <i class="fas fa-home"></i>
                    </a>
                </li>
                <li><span>Dashboard</span></li>
                <li><span>Topic</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="sidebar-right"></a>
        </div>
    </header>
    <!-- start: page -->
    <div class="row">
        <div class="col-lg-12">
            <form id="form" Method="POST" action="{{ route('createtopic') }}" class="form-horizontal">
                @csrf
                <section class="card">
                    <header class="card-header">
                        <h2 class="card-title">Add Topic</h2>
                    </header>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-3 control-label text-sm-right pt-2">Subject <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <select id="subject_id" name="subject_id" data-plugin-selectTwo class="form-control populate" title="Please select at least one Subject" required>
                                    <option value="">Choose a Subject</option>
                                    @foreach($subjects as $subject)
                                        <option value="{{ $subject->id }}">{{ $subject->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 control-label text-sm-right pt-2">Chapter <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <select id="chapter_id" name="chapter_id" class="form-control populate" title="" required>
                                    <option value="">Choose a Chapter</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 control-label text-sm-right pt-2">Title <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" name="title" class="form-control" required/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 control-label text-sm-right pt-2">is Featured? <span class="required">*</span></label>
                                <div class="col-sm-9">
                                    <input id="top_add_featured" value="0" type="checkbox" name="featured" />
                                </div>
                        </div>
                       <div class="form-group row">
                            <label class="col-sm-3 control-label text-sm-right pt-2">Content </label>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <textarea class="ckeditor form-control" name="content"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="card-footer">
                        <div class="row justify-content-end">
                            <div class="col-sm-9">
                                <button class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn btn-default">Reset</button>
                            </div>
                        </div>
                    </footer>
                </section>
            </form>
        </div>
    </div>
    <!-- end: page -->
</section>

<script src="{{asset('ckeditor/ckeditor.js')}}"></script>

{{-- <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script> --}}
<script type="text/javascript">
     $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
@endsection

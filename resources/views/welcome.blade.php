@extends('layouts.front')
@section('content')
<section>
    <?php
        if(!empty($subjectArray)) {
            //print_r($subjectArray);
            foreach($subjectArray as $resultSubjectArray) {
                // $subject_name   =   $resultSubjectArray['subject_name'];
                $subjectId      =   $resultSubjectArray['subject_id'];
                $chaptersArray  =   $resultSubjectArray['chapters'];
            }
        }
    ?>
    <div class="wrapper" style="width:100%!important; min-width:100%!important;">
        <nav id="sidebar">
            <div class="sideBarNav">
                <h3>WEB TUTORIALS</h3>
               <hr>
                <ul class="ultra" style="list-style-type:none!important;">
                    <li class="listing" ><a href="#">HTML Tutorial</a></li>
                    <li class="listing"><a href="#">CSS Tutorial</a></li>
                    <li class="listing"><a href="#">JavaScript Tutorial</a></li>
                    <li class="listing"><a href="#">jQuery Tutorial</a></li>
                    <li class="listing"><a href="#">Bootstrap Tutorial</a></li>
                    <li class="listing"><a href="#">PHP Tutorial</a></li>
                    <li class="listing"><a href="#">SQL Tutorial</a></li>
                </ul>
            </div>
        </nav>
        <!-- Page Content Holder -->
        <div id="content">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div>
                    <button type="button" id="sidebarCollapse" class="navbar-btn"> <span></span> <span></span> <span></span> </button>
                </div>
            </nav>
            <div class="conSecLeft">
                <div class="row clearfix">
                    <div class="col-lg-10 col-md-10 col-sm-7 col-xs-12 ">
                        <div class="text-center">
                            <a href="#"><img class="img-responsive" src="{{ asset('front-end/images/banner-top.jpg') }}"></a>
                        </div>
                        <?php
                            if(!empty($subjectArray)) {
                                foreach($subjectArray as $resultSubjectArray) {
                                    $chaptersArray  =   $resultSubjectArray['chapters'];
                                }
                            }
                        ?>
                        <div class="main_body_scroll">
                            <div class="row">
                                <?php
                                    if(!empty($chaptersArray)) {
                                        foreach($chaptersArray as $resultChaptersArray) {
                                            $topicArray =   $resultChaptersArray['topics'];
                                            //print_r($topicArray);
                                ?>
                                <div class="col-xs-12">
                                    <div class="white_box_border">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="main_headding">
                                                    <a href="<?php echo(url("{$subjectId}/{$resultChaptersArray['id']}"));?>" class="headding_link">
                                                        <h2><?php echo($resultChaptersArray['chapters_name']);?></h2>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <?php
                                                if(!empty($topicArray)) {
                                                    foreach($topicArray as $resultTopicArray) {
                                            ?>
                                            <div class="col-md-6"><a href="<?php echo(url("{$subjectId}/{$resultChaptersArray['id']}/{$resultTopicArray['id']}"));?>" class="new_box_main_link2"><?php echo($resultTopicArray['topic_name']);?></a></div>
                                            <?php
                                                    }
                                                }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-5 col-xs-12">
                        <div class="text-center">
                            <a href="#"><img class="img-responsive" src="{{ asset('front-end/images/banner-right.jpg') }}"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
